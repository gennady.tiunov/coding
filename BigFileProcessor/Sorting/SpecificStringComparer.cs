﻿using System.Collections.Generic;

namespace BigFileProcessor.Sorting
{
	public class SpecificStringComparer : IComparer<string>
	{
		public int Compare(string leftValue, string rightValue)
		{
			return SpecificStringComparisonHelper.Compare(leftValue, rightValue);
		}
	}
}