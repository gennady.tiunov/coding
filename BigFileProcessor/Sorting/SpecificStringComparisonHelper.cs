﻿using System;

namespace BigFileProcessor.Sorting
{
	public static class SpecificStringComparisonHelper
	{
		public static int Compare(string leftValue, string rightValue)
		{
			if (string.IsNullOrEmpty(leftValue))
			{
				throw new ArgumentNullException(nameof(leftValue));
			}

			if (string.IsNullOrEmpty(rightValue))
			{
				throw new ArgumentNullException(nameof(rightValue));
			}

			var (leftNumber, leftString) = ParseString(leftValue);
			var (rightNumber, rightString) = ParseString(rightValue);

			var stringComparisonResult = string.Compare(leftString, rightString, StringComparison.OrdinalIgnoreCase);

			return stringComparisonResult != 0 ? stringComparisonResult : leftNumber.CompareTo(rightNumber);
		}

		private static Tuple<uint, string> ParseString(string originalString)
		{
			var stringParts = originalString.Split('.');
			return new Tuple<uint, string>(uint.Parse(stringParts[0]), stringParts[1]);
		}
	}
}