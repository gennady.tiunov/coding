﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BigFileProcessor.Infrastructure;

namespace BigFileProcessor.Sorting
{
	public class FileSorter
	{
		private const long SplitFileLines = 1_000_000;

		private const string SplitFileNameTemplate = "File{0}.txt";
		private const string SortedFileName = "Sorted.txt";

		private readonly string _originalFilePath;
		private readonly string _originalFolderPath;

		private readonly ILogger _logger;

		public FileSorter(
			string filePath,
			ILogger logger)
		{
			if (string.IsNullOrEmpty(filePath))
			{
				throw new ArgumentNullException(nameof(filePath));
			}

			if (!File.Exists(filePath))
			{
				throw new ArgumentException($"File '{filePath}' does not exist!");
			}

			_originalFilePath = filePath;
			_originalFolderPath = _originalFilePath.Replace(Path.GetFileName(_originalFilePath), string.Empty);

			_logger = logger;
		}

		public void Sort()
		{
			var fileCounter = SplitOriginToSortedFiles();

			MergeSplitFiles(fileCounter);
		}

		private int SplitOriginToSortedFiles()
		{
			_logger.Write($"Splitting original file '{_originalFilePath}' - " + DateTime.Now);

			var fileCounter = 0;

			var tasks = new List<Task>();

			foreach (var chunk in GetChunks())
			{
				fileCounter++;

				_logger.Write($"Chunk {fileCounter} received - " + DateTime.Now);

				var counterClosure = fileCounter;

				tasks.Add(Task.Run(() =>
				{
					_logger.Write($"Chunk {counterClosure} - sorting started  " + DateTime.Now);
					chunk.Sort(new SpecificStringComparer());
					_logger.Write($"Chunk {counterClosure} - sorting finished " + DateTime.Now);

					_logger.Write($"Chunk {counterClosure} - writing to file started " + DateTime.Now);

					var splitFilePath = Path.Combine(_originalFolderPath, string.Format(SplitFileNameTemplate, counterClosure));

					using var writer = File.CreateText(splitFilePath);

					foreach (var line in chunk)
					{
						writer.WriteLine(line);
					}

					_logger.Write($"Chunk {counterClosure} - writing to file finished " + DateTime.Now);
				}));
			}

			Task.WaitAll(tasks.ToArray());

			return fileCounter;
		}


		private IEnumerable<List<string>> GetChunks()
		{
			using var reader = File.OpenText(_originalFilePath);

			while (!reader.EndOfStream)
			{
				var chunk = new List<string>();

				for (var lineIndex = 0; lineIndex < SplitFileLines; lineIndex++)
				{
					chunk.Add(reader.ReadLine());

					if (reader.EndOfStream)
					{
						break;
					}
				}

				yield return chunk;
			}
		}

		private void MergeSplitFiles(int fileCounter)
		{
			_logger.Write($"Sorting and merging {fileCounter} split files - " + DateTime.Now);

			var fileList = new List<StringFileReference>();

			for (var fileNumber = 1; fileNumber <= fileCounter; fileNumber++)
			{
				var splitFilePath = Path.Combine(_originalFolderPath, string.Format(SplitFileNameTemplate, fileNumber));

				var stringReference = new StringFileReference();

				var reader = File.OpenText(splitFilePath);
				if (!reader.EndOfStream)
				{
					stringReference.Reader = reader;
					stringReference.String = stringReference.Reader.ReadLine();
					fileList.Add(stringReference);
				}
			}

			using var writer = File.CreateText(Path.Combine(_originalFolderPath, SortedFileName));

			while (fileList.Any())
			{
				var minStringReference = fileList.Min();

				writer.WriteLine(minStringReference.String);

				if (!minStringReference.Reader.EndOfStream)
				{
					minStringReference.String = minStringReference.Reader.ReadLine();
				}
				else
				{
					minStringReference.Reader.Dispose();
					fileList.Remove(minStringReference);
				}
			}
		}
	}
}