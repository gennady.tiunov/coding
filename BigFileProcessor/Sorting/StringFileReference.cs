﻿using System;
using System.IO;

namespace BigFileProcessor.Sorting
{
	public class StringFileReference : IComparable<StringFileReference>
	{
		public string String { get; set; }

		public StreamReader Reader { get; set; }

		public int CompareTo(StringFileReference other)
		{
			return SpecificStringComparisonHelper.Compare(String, other.String);
		}
	}
}
