﻿using System;
using System.IO;
using BigFileProcessor.Infrastructure;

namespace BigFileProcessor.Generation
{
	public class FileGenerator : IFileGenerator
	{
		private readonly FileGeneratorSettings _settings;
		private readonly IFileLineGenerator _fileLineGenerator;
		private readonly ILogger _logger;

		public FileGenerator(
			FileGeneratorSettings settings,
			IFileLineGenerator fileLineGenerator,
			ILogger logger)
		{
			_settings = settings ?? throw new ArgumentNullException(nameof(settings));
			_fileLineGenerator = fileLineGenerator ?? throw new ArgumentNullException(nameof(fileLineGenerator));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public void GenerateFile()
		{
			_logger.Write($"Generating original file '{_settings.FilePath}' - " + DateTime.Now);

			using var streamWriter = File.CreateText(_settings.FilePath);

			for (long i = 0; i < _settings.LineCount; i++)
			{
				streamWriter.WriteLine(_fileLineGenerator.GenerateLine());
			}
		}
	}
}