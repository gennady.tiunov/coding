﻿namespace BigFileProcessor.Generation
{
	public class FileGeneratorSettings
	{
		public string FilePath { get; set; }

		public long LineCount { get;  set; }
	}
}