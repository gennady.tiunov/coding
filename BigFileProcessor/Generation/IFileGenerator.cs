﻿namespace BigFileProcessor.Generation
{
	public interface IFileGenerator
	{
		void GenerateFile();
	}
}