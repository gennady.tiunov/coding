﻿namespace BigFileProcessor.Generation
{
	public interface IFileLineGenerator
	{
		string GenerateLine();
	}
}