﻿using System;
using System.Linq;

namespace BigFileProcessor.Generation
{
	public class SpecificFileLineGenerator : IFileLineGenerator
	{
		private readonly Random _random = new Random();

		private const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		private const byte StringLength = 50;

		public string GenerateLine()
		{
			return $"{_random.Next()}.{GenerateRandomString(StringLength)}";
		}

		private string GenerateRandomString(int length)
		{
			return new string(Enumerable.Repeat(Chars, length)
				.Select(str => str[_random.Next(str.Length)])
				.ToArray());
		}
	}
}