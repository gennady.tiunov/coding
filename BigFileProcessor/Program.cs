﻿using System;
using BigFileProcessor.Generation;
using BigFileProcessor.Infrastructure;
using BigFileProcessor.Sorting;

namespace BigFileProcessor
{
	class Program
	{
		static void Main(string[] args)
		{
			var logger = new ConsoleLogger();

			var fileGeneratorSettings = new FileGeneratorSettings
			{
				FilePath = "Files\\OriginalFile.txt",
				LineCount = 10_000_000
			};
			var fileLineGenerator = new SpecificFileLineGenerator();

			var fileGenerator = new FileGenerator(fileGeneratorSettings, fileLineGenerator, logger);
			fileGenerator.GenerateFile();

			var fileSorter = new FileSorter(fileGeneratorSettings.FilePath, logger);
			fileSorter.Sort();

			logger.Write("Operation completed - " + DateTime.Now);
		}
	}
}