﻿namespace BigFileProcessor.Infrastructure
{
	public interface ILogger
	{
		void Write(string log);
	}
}