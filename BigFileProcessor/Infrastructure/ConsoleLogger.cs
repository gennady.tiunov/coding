﻿using System;

namespace BigFileProcessor.Infrastructure
{
	public class ConsoleLogger : ILogger
	{
		public void Write(string log)
		{
			Console.WriteLine(log);
		}
	}
}